use num_rational::Ratio;
use structopt::StructOpt;
use crate::colorscheme::Colorschemes;

#[derive(StructOpt)]
pub struct Args {
    #[structopt(short = "a", long = "average-cpu")]
    pub average_cpu: bool,

    #[structopt(short = "b", long = "battery")]
    pub battery: bool,

    #[structopt(
        short = "c", 
        long = "colorscheme",
        default_value = "default",
        long_help = r"Colorschemes:
        - default
        - default-dark
        - solarized-dark
        -monokai
        -vice
        "
    )]
    pub colorscheme: Colorschemes,
    #[structopt(short = "f", long = "fahrenheit")]
    pub fahrenheit: bool,

    #[structopt(short = "i", long = "interface", default_value = "all")]
    pub interface: String,

    #[structopt(short = "I", long = "interval", default_value = "1")]
    pub interval: Ratio<u64>,

    #[structopt(short = "m", long = "minimal")]
    pub minimal: bool,

    #[structopt(short = "p", long = "per-cpu")]
    pub per_cpu: bool,

    #[structopt(short = "s", long = "statusbar")]
    pub statusbar: bool,
}
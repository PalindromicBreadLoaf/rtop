# rtop

htop rewritten in rust. ~ May or may not work.

Building Instructions:

Clone this repo and move into the directory

```
git clone https://gitlab.com/PalindromicBreadLoaf/rtop.git
cd rtop
```
Install Rust compiler

For Unix-based systems
```
$ curl --proto '=https' --tlsv1.3 https://sh.rustup.rs -sSf | sh
```

For Windows
```
Install Visual Studio 2022
Follow the requirements here:
https://www.rust-lang.org/tools/install
```

Make sure Cargo is installed with
```
cargo --version
```
build
```
cargo build
```
The output is located in the target/debug directory
